

def mayor(val1, val2):
    if (val1 > val2):
        mayor = val1
    elif (val1 < val2):
        mayor = val2
    else:
        print('Ambos valores son iguales')
    return mayor
    
#(********)
#(* main *)
#(********)

if __name__ == "__main__":
    val1 = 4
    val2 = 2
    lista = [4, 7, 5, 1]
    print(mayor(lista[0], lista[1]))
    print(mayor(lista[2], lista[3]))
    print(mayor(mayor(lista[2], lista[3]), mayor(lista[0], lista[1])))